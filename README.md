# Simple NextJS App 

(API - pagination - routing) 

I did this site in order to play with `next.js` and to see what this framework is capable of 

## Tech Stack

* `AXIOS` as HTTP client (just because it is better then `isomorphic-unfetch` :) )
* `React-paginate` for pagination
* API (free API from Adzuna found [here ->](https://github.com/public-apis/public-apis#jobs))
* Routing with `useRouter` hook (access to `Router` object)
* Bootstrap as basic grid (using `@zeit/next-sass` and `nextjs-fonts`)
* Color palette used: [colors.co](https://coolors.co/c1cfda-20a4f3-59f8e8-941c2f-03191e)

![Screenshot](public/ivana-theme.jpg)

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/zeit/next.js/tree/canary/packages/create-next-app).

## Development Setup

Set up `.env` configurations. Use the `example.env` files as a guideline.

Run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/zeit/next.js/) - your feedback and contributions are welcome!

## Deploy on ZEIT Now

The easiest way to deploy your Next.js app is to use the [ZEIT Now Platform](https://zeit.co/import?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.