module.exports = {
    root: true,
    extends: 'standard',
    rules: {
        indent: [
            'error',
            4,
            {
                SwitchCase: 1
            }
        ]
    }
}
