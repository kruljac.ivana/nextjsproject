import Header from 'components/Header'
import getConfig from 'next/config'
import axios from 'axios'
import 'assets/scss/styles.scss'

//-- overwriting original app wrapper
function MyApp({ Component, pageProps, categories }) {
    // console.log(categories)
    return (
        <>
            <Header />
            <Component {...pageProps} />
        </>
    )
}

// const { publicRuntimeConfig } = getConfig()

// MyApp.getInitialProps = async () => {

//     const resp = await axios.get(`${publicRuntimeConfig.API_URL}/${publicRuntimeConfig.API_LANG}/categories?app_id=${publicRuntimeConfig.API_ID}&app_key=${publicRuntimeConfig.API_KEY}`)

//     // console.log('Here' + resp.data.results)

//     return {
//         categories: resp.data.results
//     }
// }

export default MyApp