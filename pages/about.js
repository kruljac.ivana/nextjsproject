import Head from 'next/head'

function About() {
    return (
        <div>
            <Head>
                <title>About NextJS app</title>
                <meta name="description" content="NextJS ⯈ React ✓ React-paginate ✓ AXIOS ✓ API" />
                <meta property="og:type" content="website" />
                <meta name="og:title" property="og:title" content="Stellenangebote | NextJS" />
                <meta name="og:description" property="og:description" content="NextJS ⯈ React ✓ React-paginate ✓ AXIOS ✓ API" />
                <meta property="og:site_name" content="Stellenangebote | NextJS" />
                <meta property="og:image" content={`${process.env.PUBLIC_URL}/nextjs-react-api.jpg`} />
                <meta name="twitter:card" content="summary" />
                <meta name="twitter:title" content="Stellenangebote | NextJS" />
                <meta name="twitter:description" content="NextJS ⯈ React ✓ React-paginate ✓ AXIOS ✓ API" />
                <meta property="twitter:image" content={`${process.env.PUBLIC_URL}/nextjs-react-api.jpg`} />
            </Head>
            <div className="container">
                <div className="row">
                    <div className="col-12 py-5">
                        <div className="h3 pb-3">This site is made with NextJS using:</div>
                        <ul>
                            <li className="pb-1"><code>AXIOS</code> as HTTP client (just because I like it better then <code>isomorphic-unfetch</code> :) )</li>
                            <li className="pb-1"><code>React-paginate</code> for pagination</li>
                            <li className="pb-1">API | free API from Adzuna found here » <a href="https://github.com/public-apis/public-apis#jobs" target="_blank">https://github.com/public-apis/public-apis#jobs</a></li>
                            <li className="pb-1">Routing with <code>useRouter</code> hook (access to <code>Router</code> object)</li>
                            <li className="pb-1">Bootstrap as basic grid (using <code>@zeit/next-sass</code> and <code>nextjs-fonts</code>)</li>
                            <li className="pb-1">Color palette used: <a href="https://coolors.co/c1cfda-20a4f3-59f8e8-941c2f-03191e" target="_blank">coolors.co</a></li>
                        </ul>
                        <div class="my-3">Stuff covered in this App:</div>
                        <ul>
                            <li className="pb-1">Layouts</li>
                            <li className="pb-1">Fetching the data - using <code>getServerSideProps()</code> function</li>
                            <li className="pb-1">Static Routes</li>
                            <li className="pb-1">Dynamic and Nested Routes</li>
                            <li className="pb-1">Pagination</li>
                        </ul>
                        <div className="my-3">
                            <div class="h3">Feel free to download the code and play around with it:</div>
                            <code>git clone https://gitlab.com/kruljac.ivana/nextjsproject</code>
                            <div className="py-2">
                                <a href="https://gitlab.com/kruljac.ivana/nextjsproject" target="_blank">Go to Git Project »</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default About
