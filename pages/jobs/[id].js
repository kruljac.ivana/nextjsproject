import axios from 'axios'

function Job() {
    return (
        <div className="container">
            <div className="py-3"></div>
            <div className="row">
                <div className="col-12">
                    <h1>Title</h1>
                </div>
                <div className="col-12 my-3">
                    <div className="desc">Description</div>
                </div>
            </div>
        </div>
    )
}

// export async function getServerSideProps(context) {

//     const { id } = context.query
//     // console.log(id)

//     const res = await axios.get(`${process.env.API_URL}/${process.env.API_LANG}/search?app_id=${process.env.API_ID}&app_key=${process.env.API_KEY}&id=${id}`)

//     return {
//         props: {
//             job: res.data
//         }
//     }
// }

export default Job