import Head from 'next/head'
import React, { useState, useEffect } from 'react'
import axios from 'axios'
import ReactPaginate from 'react-paginate'
import Router, { withRouter } from 'next/router'
import Loader from 'components/Loader'
import CardJobs from 'components/CardJobs'

const Home = (props) => {
    const [isLoading, setLoading] = useState(false) //State for the loading indicator
    const startLoading = () => setLoading(true)
    const stopLoading = () => setLoading(false)
    /*
        Jobs fetching happens after page navigation, 
        so we need to switch Loading state on Router events.
    */
    useEffect(() => { //After the component is mounted set router event handlers
        Router.events.on('routeChangeStart', startLoading)
        Router.events.on('routeChangeComplete', stopLoading)

        return () => {
            Router.events.off('routeChangeStart', startLoading)
            Router.events.off('routeChangeComplete', stopLoading)
        }
    }, [])

    // -- When new page selected in paggination, we take current path and query parrams.
    // -- Then add or modify page parram and then navigate to the new route.
    const pagginationHandler = (page) => {
        const currentPath = props.router.pathname
        const currentQuery = props.router.query
        currentQuery.page = page.selected + 1

        window.scrollTo(0, 0)

        props.router.push({
            pathname: currentPath,
            query: currentQuery
        })
    }

    //-- Conditional rendering of the jobs list or loading indicator
    let content = null
    if (isLoading)
        content = (
            <div className="col-12">
                <Loader />
            </div>
        )
    else {
        //-- Generating jobs list
        content = (
            <div className="row">
                {props.jobs.map(job => {
                    return (
                        <div key={job.id} className="col-md-6 col-lg-3 mb-4">
                            <CardJobs job={job} />
                        </div>
                    )
                })}
            </div>
        )
    }

    return (
        <div className="holder">
            <Head>
                <title>Stellenangebote | NextJS</title>
                <meta name="description" content="NextJS ⯈ React ✓ React-paginate ✓ AXIOS ✓ API" />
                <meta property="og:type" content="website" />
                <meta name="og:title" property="og:title" content="Stellenangebote | NextJS" />
                <meta name="og:description" property="og:description" content="NextJS ⯈ React ✓ React-paginate ✓ AXIOS ✓ API" />
                <meta property="og:site_name" content="Stellenangebote | NextJS" />
                <meta property="og:image" content={`${process.env.PUBLIC_URL}/nextjs-react-api.jpg`} />
                <meta name="twitter:card" content="summary" />
                <meta name="twitter:title" content="Stellenangebote | NextJS" />
                <meta name="twitter:description" content="NextJS ⯈ React ✓ React-paginate ✓ AXIOS ✓ API" />
                <meta property="twitter:image" content={`${process.env.PUBLIC_URL}/nextjs-react-api.jpg`} />
            </Head>
            <div className="pb-5">
                <div className="subheader">
                    <div className="container">
                        <h1 className="headline text-left mx-auto mt-0 pt-4 pb-2">
                            Alle Stellenangebote
                        </h1>
                    </div>
                </div>
            </div>
            <div className="container">
                {content}
                <div className="py-3"></div>
                <div className="row">
                    <div className="col-12">
                        <ReactPaginate
                            previousLabel={'«'}
                            nextLabel={'»'}
                            nextClassName={'page-item'}
                            nextLinkClassName={'page-link'}
                            previousClassName={'page-item'}
                            previousLinkClassName={'page-link'}
                            breakLabel={'...'}
                            breakClassName={'page-link'}
                            activeClassName={'active'}
                            containerClassName={'float-right pagination'}
                            pageClassName={'page-item'}
                            pageLinkClassName={'page-link'}
                            initialPage={props.currentPage - 1}
                            pageCount={props.pageCount}
                            marginPagesDisplayed={1}
                            pageRangeDisplayed={1}
                            onPageChange={pagginationHandler}
                        />
                    </div>
                </div>
            </div>
            <div className="py-5"></div>
            <div className="py-3"></div>
        </div >
    )
}

export async function getServerSideProps({ query }) {
    const page = query.page || 1
    const res = await axios.get(`${process.env.API_URL}/${process.env.API_LANG}/search/${page}?app_id=${process.env.API_ID}&app_key=${process.env.API_KEY}&results_per_page=20`)
    // console.log(res.data)
    return {
        props: {
            totalCount: res.data.count,
            pageCount: Math.round(res.data.count / 20),
            currentPage: '1',
            perPage: '20',
            jobs: res.data.results
        }
    }
}

export default withRouter(Home)
