import Link from 'next/link'
import { useRouter } from 'next/router'

function Navigation() {
    const router = useRouter()
    // console.log(router)

    return (
        <nav>
            <ul className="list-unstyled mb-0 text-center text-md-right">
                <li>
                    <Link href="/">
                        <a className={router.pathname === '/' ? 'active' : ''}>Jobs</a>
                    </Link>
                </li>
                <li>
                    <Link href="/it">
                        <a className={router.pathname === '/it' ? 'active' : ''}>IT Jobs</a>
                    </Link>
                </li>

                <li>
                    <Link href="/about">
                        <a className={router.pathname === '/about' ? 'active' : ''}>About</a>
                    </Link>
                </li>
            </ul>
        </nav>
    )
}

export default Navigation


