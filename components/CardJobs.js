import Link from 'next/link'
import Location from 'components/svg/Location'

function CardJobs({ job }) {
    return (
        <div className="card jobcard h-100">
            <div className="card-header text-center">
                {job.title}
            </div>
            <div className="card-body p-3">
                <div className="font-weight-bold pb-2">
                    {job.company.display_name}
                </div>
                <div className="category mb-2">
                    {job.category.label}
                </div>
                <div>
                    {/* return first 25 words */}
                    {job.description
                        .split(' ')
                        .slice(0, 25)
                        .join(' ')}
                     &nbsp;...
                </div>
            </div>
            <div className="card-footer">
                <div className="row align-items-center">
                    <div className="col-md-7 pr-0">
                        <Location />
                        {job.location.display_name}
                    </div>
                    <div className="col-md-5 text-right">
                        {/* Dynamic route */}
                        {/* <Link href="/jobs/[id]" as={`jobs/${job.id}`}>
                            <a>Show me »</a>
                        </Link> */}
                        <a href={job.redirect_url} target="_blank">Show me »</a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CardJobs