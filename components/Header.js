import Link from 'next/link'
import Navigation from 'components/Navigation'

function Header() {
    return (
        <header className="py-2">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-md-4">
                        <Link href="https://ivanakruljac.netlify.app">
                            <a className="ivana" target="_blank">
                                Ivana<span>_</span>
                            </a>
                        </Link>
                    </div>
                    <div className="col-md-8">
                        <Navigation />
                    </div>
                </div>

            </div>
        </header>
    )
}

export default Header
