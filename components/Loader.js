export default function Loader() {
    return (
        <div>
            <div className="spinner-wrapper">
                <div className="spinner">
                    <div className="double-bounce1"></div>
                    <div className="double-bounce2"></div>
                </div>
            </div>

            <style jsx>{`
                .spinner-wrapper {
                    height: 100vh;
                    padding-top: 50px;
                    width: 100%;
                }
                .spinner {
                    width: 100px;
                    height: 100px;
                    position: relative;
                    margin: 0 auto;
                }
                .double-bounce1, .double-bounce2 {
                    width: 100%;
                    height: 100%;
                    border-radius: 50%;
                    background-color: #d50000;
                    opacity: 0.6;
                    position: absolute;
                    top: 0;
                    left: 0;
                    animation: sk-bounce 2s infinite ease-in-out;
                }
                .double-bounce2 {
                    -webkit-animation-delay: -1s;
                    animation-delay: -1s;
                }
                @keyframes sk-bounce {
                    0%,
                    100% {
                        transform: scale(0);
                    }
                    50% {
                        transform: scale(1);
                    }
                }
                .infinite-status-prompt {
                    color: rgba(#d50000, 0.65);
                    font-size: 0.8rem;
                    margin: 64px auto;
                    text-align: center;
                }
            `}</style>
        </div>
    )
}