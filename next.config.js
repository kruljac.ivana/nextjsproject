const path = require('path')
const useSass = require('@zeit/next-sass')
const useFonts = require('nextjs-fonts')

// evironmental variables
require('dotenv').config()

module.exports = useSass(
    useFonts({
        env: {
            API_URL: process.env.API_URL,
            API_LANG: process.env.API_LANG,
            API_ID: process.env.API_ID,
            API_KEY: process.env.API_KEY
        },
        publicRuntimeConfig: {
            API_URL: process.env.API_URL,
            API_LANG: process.env.API_LANG,
            API_ID: process.env.API_ID,
            API_KEY: process.env.API_KEY
        },
        webpack: (config) => {
            config.resolve.alias['components'] = path.join(
                __dirname,
                'components'
            )
            config.resolve.alias['assets'] = path.join(__dirname, 'assets')
            config.resolve.alias['public'] = path.join(__dirname, 'public')

            return config
        },
    })
)
